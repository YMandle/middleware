const express = require('express');
const db = require('./db');
const utils =  require('./utils');
const multer = require('multer');
const upload = multer({ dest: 'images/' })


const router = express.Router();

router.get('/trek', (request, response) => {
    const connection = db.connect();
    const statement = `select Tid, Tname, Date, Seats,Duration , Type , Difficulty, Amount,Description,Region,Thumbnail from TrekSchedule`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/trek/:Tid', (request, response) => {
    const Tid = request.params.Tid;
    const connection = db.connect();
    const statement = `select Tid, Tname, Date, Seats,Duration , Type , Difficulty, Amount,Description,Region,Thumbnail from TrekSchedule where Tid = ${Tid}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result[0]));
    })
});

router.post('/trek', upload.single('Thumbnail'), (request, response) => {
   
    const Tname = request.body.Tname;
    const Date = request.body.Date;
    const Seats = request.body.Seats;
    const Duration = request.body.Duration;
    const Type = request.body.Type;
    const Difficulty = request.body.Difficulty;
    const Amount = request.body.Amount;
    const Description = request.body.Description;
    const Region = request.body.Region;
    const Thumbnail = request.file.filename;
    
    const connection = db.connect();
    const statement = `insert into TrekSchedule (Tname,Date, Seats,Duration , Type , Difficulty, Amount,Description,Region,Thumbnail) values( '${Tname}', '${Date}','${Seats}' , '${Duration}' ,'${Type}','${Difficulty}', '${Amount}','${Description}','${Region}','${Thumbnail}')`;
    connection.query( statement , (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.put('/trek/:Tid', (request, response) => {
    const Tid = request.params.Tid;
    const {Tname,Date, Seats,Duration , Type , Difficulty, Amount,Description,Region,Thumbnail} = request.body;
    const connection = db.connect();
    const statement = `update TrekSchedule
        set
            Tname = '${Tname}',
            Date = '${Date}',
            Seats = ${Seats},
            Duration = '${Duration}',
            Type = '${Type}',
            Difficulty = '${Difficulty}',
            Amount = ${Amount},
            Description = '${Description}',
            Region = '${Region}',
            Thumbnail = '${Thumbnail}'
        where Tid = ${Tid}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/trek/:Tid', (request, response) => {
    const Tid = request.params.Tid;
    const connection = db.connect();
    const statement = `delete from TrekSchedule where Tid = ${Tid}`;
    console.log("Tid"+Tid)

    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});


module.exports = router;